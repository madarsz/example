<?php
namespace console\modules\sitemap\controllers;

use console\modules\sitemap\actions\incremental\AdviceIncrementalAction;
use console\modules\sitemap\actions\incremental\BranchIncrementalAction;
use console\modules\sitemap\actions\incremental\CategoryIncrementalAction;
use console\modules\sitemap\actions\incremental\CinemaBasicIncrementalAction;
use console\modules\sitemap\actions\incremental\CinemaMovieIncrementalAction;
use console\modules\sitemap\actions\incremental\CompanyIncrementalAction;
use console\modules\sitemap\actions\incremental\CouponIncrementalAction;
use console\modules\sitemap\actions\incremental\CurrencyRatesIncrementalAction;
use console\modules\sitemap\actions\incremental\EntertainmentIncrementalAction;
use console\modules\sitemap\actions\incremental\EventIncrementalAction;
use console\modules\sitemap\actions\incremental\LursoftCompanyIncrementalAction;
use console\modules\sitemap\actions\incremental\NameDayIncrementalAction;
use console\modules\sitemap\actions\incremental\TransportCityIncrementalAction;
use console\modules\sitemap\actions\incremental\TransportIntercityIncrementalAction;
use console\modules\sitemap\actions\incremental\VacancyCompanyIncrementalAction;
use mgcode\commandLogger\LoggingTrait;
use yii\console\Controller;

class IncrementalController extends Controller
{
    use LoggingTrait;

    public function actions()
    {
        return [
            'transport-city' => ['class' => TransportCityIncrementalAction::className(),],
            'transport-intercity' => ['class' => TransportIntercityIncrementalAction::className(),],
            'coupon' => ['class' => CouponIncrementalAction::className(),],
            'entertainment' => ['class' => EntertainmentIncrementalAction::className()],
            'cinema-movie' => ['class' => CinemaMovieIncrementalAction::className()],
            'cinema-basic' => ['class' => CinemaBasicIncrementalAction::className()],
            'event' => ['class' => EventIncrementalAction::className()],
            'name-day' => ['class' => NameDayIncrementalAction::className()],
            'vacancy-company' => ['class' => VacancyCompanyIncrementalAction::className()],
            'currency-rates' => ['class' => CurrencyRatesIncrementalAction::className()],
            'advice' => ['class' => AdviceIncrementalAction::className()],
            'category' => ['class' => CategoryIncrementalAction::className()],
            'lursoft' => ['class' => LursoftCompanyIncrementalAction::className()],
            'company' => ['class' => CompanyIncrementalAction::className()],
            'branch' => ['class' => BranchIncrementalAction::className()],
        ];
    }

    /**
     * Runs all incremental actions
     */
    public function actionIndex()
    {
        try {
            $this->msg('Starting all incremental actions');
            $this->runAction('transport-city');
            $this->runAction('transport-intercity');
            $this->runAction('coupon');
            $this->runAction('entertainment');
            $this->runAction('cinema-movie');
            $this->runAction('cinema-basic');
            $this->runAction('event');
            $this->runAction('name-day');
            $this->runAction('vacancy-company');
            $this->runAction('currency-rates');
            $this->runAction('advice');
            $this->runAction('category');
            $this->runAction('lursoft');
            $this->runAction('company');
            $this->runAction('branch');
            $this->msg('Finished all incremental actions');
        } catch (\Exception $e) {
            $this->logException($e);
        }
    }
}
