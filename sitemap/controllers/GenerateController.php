<?php
namespace console\modules\sitemap\controllers;

use common\modules\sitemap\models\Sitemap;
use mgcode\commandLogger\LoggingTrait;
use yii\console\Controller;

class GenerateController extends Controller
{
    use LoggingTrait;

    /**
     * Generates all sitemaps
     */
    public function actionIndex()
    {
        try {
            $this->msg('Starting generation of all sitemaps');
            $sitemaps = Sitemap::find()->andWhere(['is_deleted' => 0])->all();
            foreach ($sitemaps as $sitemap) {
                /** $sitemap Sitemap */
                $this->msg("Starting generation of sitemap with id: {$sitemap->id}");
                $sitemap->generate();
            }
        } catch (\Exception $e) {
            $this->logException($e);
        }
    }

    /**
     * Generates sitemap for specified sitemap's id as first parameter
     * @param $id
     */
    public function actionSitemap($id)
    {
        try {
            $sitemap = Sitemap::findOne($id);
            if (!$sitemap) {
                $this->msg("No sitemap found for id: {$id}");
                return;
            }
            $sitemap->generate();
        } catch (\Exception $e) {
            $this->logException($e);
        }
    }

    /**
     * Regenerates all company and lursoft company sitemaps
     */
    public function actionCompanies()
    {
        try {
            $this->msg('Starting generation of all company and lursoft company sitemaps');
            $sitemaps = Sitemap::find()->andWhere(['is_deleted' => 0, 'type' => [Sitemap::TYPE_LURSOFT_COMPANY, Sitemap::TYPE_COMPANY]])->all();
            foreach ($sitemaps as $sitemap) {
                /** $sitemap Sitemap */
                $this->msg("Starting generation of sitemap with id: {$sitemap->id}");
                $sitemap->generate();
            }
        } catch (\Exception $e) {
            $this->logException($e);
        }
    }
}
