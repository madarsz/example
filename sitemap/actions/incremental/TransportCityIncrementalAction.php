<?php

namespace console\modules\sitemap\actions\incremental;

use common\modules\sitemap\models\Sitemap;

class TransportCityIncrementalAction extends BaseIncrementalAction
{
    /**
     * Generates sitemap for city's transport
     */
    public function run()
    {
        $this->msg("Starting sitemap generation for city's transport");
        $sitemap = Sitemap::find()->andWhere(['type' => Sitemap::TYPE_TRANSPORT_CITY, 'is_deleted' => 0])->one();
        if (!$sitemap) {
            $sitemap = new Sitemap();
            $sitemap->type = Sitemap::TYPE_TRANSPORT_CITY;
            $sitemap->save();
        }
        $sitemap->generate();
    }
}