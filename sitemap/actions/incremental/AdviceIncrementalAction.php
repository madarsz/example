<?php

namespace console\modules\sitemap\actions\incremental;

use common\modules\sitemap\models\Sitemap;

class AdviceIncrementalAction extends BaseIncrementalAction
{
    /**
     * Generates sitemap for advices
     */
    public function run()
    {
        $this->msg("Starting sitemap generation for advices");
        $sitemap = Sitemap::find()->andWhere(['type' => Sitemap::TYPE_ADVICE, 'is_deleted' => 0])->one();
        if (!$sitemap) {
            $sitemap = new Sitemap();
            $sitemap->type = Sitemap::TYPE_ADVICE;
            $sitemap->save();
        }
        $sitemap->generate();
    }
}