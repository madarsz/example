<?php

namespace console\modules\sitemap\actions\incremental;

use mgcode\commandLogger\LoggingTrait;
use yii\base\Action;

abstract class BaseIncrementalAction extends Action
{
    use LoggingTrait;
}