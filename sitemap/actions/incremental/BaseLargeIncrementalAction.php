<?php

namespace console\modules\sitemap\actions\incremental;

use common\modules\sitemap\models\Sitemap;
use mgcode\helpers\TimeHelper;
use yii\db\Connection;
use yii\db\Query;

abstract class BaseLargeIncrementalAction extends BaseIncrementalAction
{
    protected $counter;
    protected $total;
    /** @var  $sitemaps Sitemap[] */
    protected $sitemaps;
    /** @var  $db Connection */
    protected $db;
    protected $type;
    /** @var  string */
    protected $tableName;
    /** @var  string[] */
    protected $columnNames;
    /** @var  $query Query */
    protected $query;
    /** @var  string */
    protected $startMessage;
    protected $batchSize;
    protected $linkLimit;

    abstract protected function getType();
    abstract protected function getTableName();
    /**
     * Method should return array of columnNames, these column names have to be the same that query returns
     * @return array
     */
    abstract protected function getColumnNames();

    /**
     * Method should return Query that returns columns with names that are equal to the ones used in table
     * which is defined in getTableName
     * @return Query
     */
    abstract protected function getQuery();
    abstract protected function getStartMessage();
    abstract protected function getBatchSize();
    abstract protected function getLinkLimit();

    /**
     * Maps new links and regenerates updated sitemaps
     * @throws \Exception
     */
    public function run()
    {
        $this->type = $this->getType();
        $this->tableName = $this->getTableName();
        $this->columnNames = $this->getColumnNames();
        $this->query = $this->getQuery();
        $this->startMessage = $this->getStartMessage();
        $this->batchSize = $this->getBatchSize();
        $this->linkLimit = $this->getLinkLimit();
        if (!$this->type) {
            throw new \Exception('type is not set');
        }
        if (!$this->tableName) {
            throw new \Exception('tableName is not set');
        }
        if (!$this->columnNames) {
            throw new \Exception('columnNames is not set');
        }
        if (!$this->query) {
            throw new \Exception('query is not set');
        }
        if (!$this->startMessage) {
            throw new \Exception('startMessage is not set');
        }
        $this->msg($this->startMessage);
        $this->initAction();
        $updatedSitemaps = [];
        $count = 0;
        $batchCount = 0;
        $columnString = $this->getColumnString();
        foreach ($this->query->batch($this->batchSize) as $links) {
            $newLinks = 0;
            $sql = "INSERT INTO {$this->tableName} ({$columnString}sitemap_id, created, updated) VALUES ";
            foreach ($links as $link) {
                $sitemap = $this->getSitemap();
                $now = TimeHelper::getTime();
                $sqlLink = $this->getSqlForLink($link);
                $sql .= "({$sqlLink}{$sitemap->id}, '{$now}', '{$now}'),";
                $sitemap->save();
                $this->sitemaps[$this->counter]['count'] += 3;
                if (!array_key_exists($sitemap->id, $updatedSitemaps)) {
                    $updatedSitemaps[$sitemap->id] = $sitemap;
                }
                $newLinks += 3;
            }
            if ($newLinks) {
                $sql = substr($sql, 0, -1);
                $this->db->createCommand($sql)->execute();
            }
            $batchCount++;
            $count += $newLinks;
            $this->msg("$count new links registered, batch cycles went through: $batchCount");
        }
        foreach ($updatedSitemaps as $sitemap) {
            $sitemap->generate();
        }
    }

    protected function initAction()
    {
        if (!$this->batchSize) {
            $this->batchSize = 100;
        }
        if (!$this->linkLimit) {
            $this->linkLimit = 14998;
        }
        $sitemaps = Sitemap::find()->andWhere(['type' => $this->type, 'is_deleted' => 0])->all();
        if (empty($sitemaps)) {
            $sitemap = new Sitemap();
            $sitemap->type = $this->type;
            $sitemap->save();
            $sitemaps[] = $sitemap;
        }
        $count = 0;
        foreach ($sitemaps as $sitemap) {
            $linkCount = (int) (new Query())->from($this->tableName)->andWhere(['sitemap_id' => $sitemap->id])->count();
            $linkCount *= 3;
            $this->sitemaps[$count] = ['sitemap' => $sitemap, 'count' => $linkCount];
            $count++;
        }
        $this->counter = 0;
        $this->total = count($this->sitemaps);
        $this->db = \Yii::$app->db;
    }


    /**
     * @return Sitemap
     */
    protected function getSitemap()
    {
        while ($this->counter < $this->total) {
            $sitemap = $this->sitemaps[$this->counter]['sitemap'];
            $count = $this->sitemaps[$this->counter]['count'];
            if ($count < $this->linkLimit) {
                return $sitemap;
            }
            $this->counter++;
        }
        $sitemap = new Sitemap();
        $sitemap->type = $this->type;
        $sitemap->save();
        $this->sitemaps[$this->counter] = ['sitemap' => $sitemap, 'count' => 0];
        $this->total++;
        return $sitemap;
    }

    protected function getColumnString()
    {
        $string = '';
        foreach ($this->columnNames as $columnName) {
            $string .= $columnName.', ';
        }
        return $string;
    }

    protected function getSqlForLink($link)
    {
        $string = '';
        foreach ($this->columnNames as $columnName) {
            $value = $this->db->quoteValue($link[$columnName]);
            $string .= $value.', ';
        }
        return $string;
    }
}