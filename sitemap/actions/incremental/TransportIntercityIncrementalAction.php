<?php

namespace console\modules\sitemap\actions\incremental;

use common\modules\sitemap\models\Sitemap;
use yii\db\Query;

class TransportIntercityIncrementalAction extends BaseLargeIncrementalAction
{
    protected function getType()
    {
        return Sitemap::TYPE_TRANSPORT_INTERCITY;
    }

    protected function getTableName()
    {
        return 'sitemap_transport_intercity';
    }

    protected function getColumnNames()
    {
        return ['from_id', 'to_id'];
    }

    protected function getQuery()
    {
        return (new Query())
            ->select(['from_id' => 'f.stop_id', 'to_id' => 't.stop_id'])
            ->distinct()
            ->from('transport_schedule_stop f')
            ->leftJoin('transport_schedule_stop t', 'f.schedule_id = t.schedule_id AND f.order < t.order')
            ->leftJoin('transport_schedule sch', 'sch.id = f.schedule_id')
            ->leftJoin('transport_route r', 'sch.route_id = r.id')
            ->leftJoin('sitemap_transport_intercity sti', 'sti.from_id = f.stop_id AND sti.to_id = t.stop_id')
            ->andWhere(['not', ['t.stop_id' => null]])
            ->andWhere(['sti.from_id' => null])
            ->andWhere(['r.is_intercity' => 1]);
    }

    protected function getStartMessage()
    {
        return "Starting sitemap incremental addition for intercity's transport";
    }

    protected function getBatchSize()
    {
        return 1000;
    }

    protected function getLinkLimit()
    {
        return 14998;
    }

    /**
     * Maps new intercity transport routes and regenerates updated sitemaps
     */
    public function run()
    {
        parent::run();
    }
}