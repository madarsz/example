<?php

namespace common\modules\sitemap\models;

use common\modules\sitemap\components\AdviceGenerate;
use common\modules\sitemap\components\BranchGenerate;
use common\modules\sitemap\components\CategoryGenerate;
use common\modules\sitemap\components\CinemaBasicGenerate;
use common\modules\sitemap\components\CinemaMovieGenerate;
use common\modules\sitemap\components\CompanyGenerate;
use common\modules\sitemap\components\CouponGenerate;
use common\modules\sitemap\components\CurrencyRatesGenerate;
use common\modules\sitemap\components\EntertainmentGenerate;
use common\modules\sitemap\components\EventGenerate;
use common\modules\sitemap\components\LursoftCompanyGenerate;
use common\modules\sitemap\components\NameDayGenerate;
use common\modules\sitemap\components\TransportCityGenerate;
use common\modules\sitemap\components\TransportIntercityGenerate;
use common\modules\sitemap\components\TransportStaticGenerate;
use common\modules\sitemap\components\VacancyCompanyGenerate;
use common\modules\sitemap\components\WeatherCityGenerate;
use mgcode\commandLogger\LoggingTrait;
use mgcode\helpers\DbHelper;
use mgcode\helpers\TimeHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "sitemap".
 */
class Sitemap extends AbstractSitemap
{
    use LoggingTrait;

    const TYPE_TRANSPORT_CITY = 1;
    const TYPE_TRANSPORT_INTERCITY = 2;
    const TYPE_TRANSPORT_STATIC = 3;
    const TYPE_WEATHER_CITY = 4;
    const TYPE_COUPON = 5;
    const TYPE_ENTERTAINMENT = 6;
    const TYPE_CINEMA_MOVIE = 7;
    const TYPE_CINEMA_BASIC = 8;
    const TYPE_EVENT = 9;
    const TYPE_NAME_DAYS = 10;
    const TYPE_VACANCY_COMPANY = 11;
    const TYPE_CURRENCY_RATES = 12;
    const TYPE_ADVICE = 13;
    const TYPE_CATEGORIES = 14;
    const TYPE_LURSOFT_COMPANY = 15;
    const TYPE_COMPANY = 16;
    const TYPE_BRANCH = 17;

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            TimestampBehavior::className() => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Flags record as deleted
     * @inheritdoc
     */
    public function delete()
    {
        $this->is_deleted = 1;
        $this->deleted = new Expression('NOW()');
        $this->updated = new Expression('NOW()');
        if (!$this->save()) {
            DbHelper::throwSaveException($this);
        }
        return true;
    }

    /** @inheritdoc */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => ['id', 'path', 'link_count', 'type'],
        ];
    }

    public function getLink()
    {
        if (!$this->path) {
            return false;
        }
        $app = \Yii::$app;
        $component = $app->has('frontendUrlManager') ? $app->frontendUrlManager : $app->urlManager;
        $filename = substr(strrchr($this->path, "/"), 1);
        $params = ['/sitemap/sitemap/view', 'filename' => $filename];
        return $component->createAbsoluteUrl($params);
    }

    public function getLastMod()
    {
        return TimeHelper::getIso8601Date(strtotime($this->updated));
    }

    public function generate()
    {
        switch ($this->type) {
            case self::TYPE_TRANSPORT_CITY:
                $class = new TransportCityGenerate($this);
                break;
            case self::TYPE_TRANSPORT_INTERCITY:
                $class = new TransportIntercityGenerate($this);
                break;
            case self::TYPE_TRANSPORT_STATIC:
                $class = new TransportStaticGenerate($this);
                break;
            case self::TYPE_WEATHER_CITY:
                $class = new WeatherCityGenerate($this);
                break;
            case self::TYPE_COUPON:
                $class = new CouponGenerate($this);
                break;
            case self::TYPE_ENTERTAINMENT:
                $class = new EntertainmentGenerate($this);
                break;
            case self::TYPE_CINEMA_MOVIE:
                $class = new CinemaMovieGenerate($this);
                break;
            case self::TYPE_CINEMA_BASIC:
                $class = new CinemaBasicGenerate($this);
                break;
            case self::TYPE_EVENT:
                $class = new EventGenerate($this);
                break;
            case self::TYPE_NAME_DAYS:
                $class = new NameDayGenerate($this);
                break;
            case self::TYPE_VACANCY_COMPANY:
                $class = new VacancyCompanyGenerate($this);
                break;
            case self::TYPE_CURRENCY_RATES:
                $class = new CurrencyRatesGenerate($this);
                break;
            case self::TYPE_ADVICE:
                $class = new AdviceGenerate($this);
                break;
            case self::TYPE_CATEGORIES:
                $class = new CategoryGenerate($this);
                break;
            case self::TYPE_LURSOFT_COMPANY:
                $class = new LursoftCompanyGenerate($this);
                break;
            case self::TYPE_COMPANY:
                $class = new CompanyGenerate($this);
                break;
            case self::TYPE_BRANCH:
                $class = new BranchGenerate($this);
                break;
            default:
                throw new \Exception('Unsupported type for sitemap with id: '.$this->id);
        }
        $class->generate();
    }
}
