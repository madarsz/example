<?php

namespace common\modules\sitemap\models;

use Yii;

/**
 * This is the model class for table "sitemap".
 *
 * @property integer $id
 * @property string $path
 * @property integer $link_count
 * @property integer $type
 * @property string $created
 * @property string $updated
 * @property string $deleted
 * @property integer $is_deleted
 */
abstract class AbstractSitemap extends \yii\db\ActiveRecord
{
    /** @inheritdoc */
    public static function tableName()
    {
        return 'sitemap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'string'],
            [['link_count', 'type', 'is_deleted'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['updated'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'link_count' => 'Link Count',
            'type' => 'Type',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\modules\sitemap\models\queries\SitemapQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\sitemap\models\queries\SitemapQuery(get_called_class());
    }
}
