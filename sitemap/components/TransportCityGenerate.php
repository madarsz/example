<?php
namespace common\modules\sitemap\components;

use common\modules\transport\models\TransportCity;
use common\modules\transport\models\TransportCityDirection;

class TransportCityGenerate extends BaseGenerate
{
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        $directions = TransportCityDirection::find()->andWhere(['is_deleted' => 0])->andWhere(['>', 'schedule_count', 0])->all();
        $cities = TransportCity::find()->visible()->sort()->all();
        $links = [];
        foreach ($cities as $city) {
            $cityLv = $city->getUrl(['lang' => 'lv'], true, 'https');
            $cityRu = $city->getUrl(['lang' => 'ru'], true, 'https');
            $cityEn = $city->getUrl(['lang' => 'en'], true, 'https');
            $links[$cityLv] = ['lv' => $cityLv, 'ru' => $cityRu, 'en' => $cityEn];
            $links[$cityRu] = ['lv' => $cityLv, 'ru' => $cityRu, 'en' => $cityEn];
            $links[$cityEn] = ['lv' => $cityLv, 'ru' => $cityRu, 'en' => $cityEn];
        }
        foreach ($directions as $direction) {
            $directionLv = $direction->getUrl(['lang' => 'lv'], true, 'https');
            $directionRu = $direction->getUrl(['lang' => 'ru'], true, 'https');
            $directionEn = $direction->getUrl(['lang' => 'en'], true, 'https');
            $links[$directionLv] = ['lv' => $directionLv, 'ru' => $directionRu, 'en' => $directionEn];
            $links[$directionRu] = ['lv' => $directionLv, 'ru' => $directionRu, 'en' => $directionEn];
            $links[$directionEn] = ['lv' => $directionLv, 'ru' => $directionRu, 'en' => $directionEn];
        }
        return $links;
    }
}