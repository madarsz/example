<?php
namespace common\modules\sitemap\components;

use common\modules\entertainment\models\Advice;
use common\modules\entertainment\models\AdviceTag;

class AdviceGenerate extends BaseGenerate
{
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        $app = \Yii::$app;
        $component = $app->has('frontendUrlManager') ? $app->frontendUrlManager : $app->urlManager;
        $links = [];

        $landingParams = ['/entertainment/advice/index'];
        $landingLv = $component->createAbsoluteUrl(array_merge($landingParams, ['lang' => 'lv']), 'https');
        $landingRu = $component->createAbsoluteUrl(array_merge($landingParams, ['lang' => 'ru']), 'https');
        $landingEn = $component->createAbsoluteUrl(array_merge($landingParams, ['lang' => 'en']), 'https');
        $links[$landingLv] = ['lv' => $landingLv, 'ru' => $landingRu, 'en' => $landingEn];
        $links[$landingRu] = ['lv' => $landingLv, 'ru' => $landingRu, 'en' => $landingEn];
        $links[$landingEn] = ['lv' => $landingLv, 'ru' => $landingRu, 'en' => $landingEn];

        $tags = AdviceTag::find()->notDeleted()->active()->all();
        $tagParams = ['/entertainment/advice/index'];
        foreach ($tags as $tag) {
            $linkLv = $component->createAbsoluteUrl(array_merge($tagParams, ['slug' => $tag->getTranslatedAttribute('slug', 'lv'), 'id' => $tag->id, 'lang' => 'lv']), 'https');
            $linkRu = $component->createAbsoluteUrl(array_merge($tagParams, ['slug' => $tag->getTranslatedAttribute('slug', 'ru'), 'id' => $tag->id, 'lang' => 'ru']), 'https');
            $linkEn = $component->createAbsoluteUrl(array_merge($tagParams, ['slug' => $tag->getTranslatedAttribute('slug', 'en'), 'id' => $tag->id, 'lang' => 'en']), 'https');
            $links[$linkLv] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
            $links[$linkRu] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
            $links[$linkEn] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
        }

        $advices = Advice::find()->active()->all();
        $adviceParams = ['/entertainment/advice/view'];
        foreach ($advices as $advice) {
            $linkLv = $component->createAbsoluteUrl(array_merge($adviceParams, ['slug' => $advice->getTranslatedAttribute('slug', 'lv'), 'id' => $advice->id, 'lang' => 'lv']), 'https');
            $linkRu = $component->createAbsoluteUrl(array_merge($adviceParams, ['slug' => $advice->getTranslatedAttribute('slug', 'ru'), 'id' => $advice->id, 'lang' => 'ru']), 'https');
            $linkEn = $component->createAbsoluteUrl(array_merge($adviceParams, ['slug' => $advice->getTranslatedAttribute('slug', 'en'), 'id' => $advice->id, 'lang' => 'en']), 'https');
            $links[$linkLv] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
            $links[$linkRu] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
            $links[$linkEn] = ['lv' => $linkLv, 'ru' => $linkRu, 'en' => $linkEn];
        }
        return $links;
    }
}