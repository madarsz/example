<?php
namespace common\modules\sitemap\components;

use common\modules\sitemap\models\Sitemap;
use mgcode\commandLogger\LoggingTrait;
use mgcode\helpers\TimeHelper;
use yii\base\Component;

abstract class BaseGenerate extends Component
{
    use LoggingTrait;
    /** @var  $sitemap Sitemap */
    protected $sitemap;

    /**
     * Function should return array of following structure:
     * [
     *      'urlLV' => ['lv' => 'urlLV', 'ru' => 'urlRU', 'en' => 'urlEN'],
     *      'urlRU' => ['lv' => 'urlLV', 'ru' => 'urlRU', 'en' => 'urlEN'],
     *      'urlEN' => ['lv' => 'urlLV', 'ru' => 'urlRU', 'en' => 'urlEN'],
     *      'urlLV2' => ['lv' => 'urlLV2', 'ru' => 'urlRU2', 'en' => 'urlEN2'],
     *      ...
     * ]
     * @return array
     */
    abstract function getLinks();

    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;
        parent::__construct();
    }

    /**
     * Function generates sitemap file
     * @throws \Exception
     */
    public function generate()
    {
        $links = $this->getLinks();
        $xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
        $xmldata .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\r\n\txmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\r\n";
        foreach ($links as $link => $alternateLinks) {
            $xmldata .= "\t<url>\r\n";
            $xmldata .= "\t\t<loc>".$link."</loc>\r\n";
            foreach ($alternateLinks as $language => $alternateLink) {
                $xmldata .= "\t\t\t<xhtml:link rel=\"alternate\" hreflang=\"".$language."\" href=\"".$alternateLink."\" />\r\n";
            }
            $xmldata .= "\t</url>\r\n";
        }
        $xmldata .= '</urlset>';

        if (!isset(\Yii::$app->params['sitemapsDir'])) {
            throw new \Exception('Parameter sitemapsDir is not set');
        }
        $path = '/tmp/sitemap-'.$this->sitemap->id.'.xml';
        if(file_put_contents($path ,$xmldata))
        {
            try {
                //Now compress to .gz
                $pathGz = \Yii::$app->params['sitemapsDir'].'sitemap-'.$this->sitemap->id.'.xml.gz';
                file_put_contents($pathGz, gzencode(file_get_contents($path)));
                $this->sitemap->path = $pathGz;
                $this->sitemap->link_count = count($links);
                $this->sitemap->updated = TimeHelper::getTime();
                $this->sitemap->save();
                $this->msg('Sitemap created: '.$pathGz);
                unlink($path);
            } catch (\Exception $e) {
                $this->logException($e);
            }
        } else {
            $this->logException(new \Exception('Failed to create file for sitemap with id: '.$this->id.' and path: '.$path));
        }
    }
}