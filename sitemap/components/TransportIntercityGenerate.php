<?php
namespace common\modules\sitemap\components;

use common\modules\transport\models\TransportRoute;
use common\modules\transport\models\TransportTrainBusStop;
use mgcode\helpers\UrlHelper;
use yii\db\Expression;
use yii\db\Query;
use yii\web\UrlManager;

class TransportIntercityGenerate extends BaseGenerate
{
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        $app = \Yii::$app;
        /** @var UrlManager $urlManager */
        $urlManager = $app->has('frontendUrlManager') ? $app->frontendUrlManager : $app->urlManager;
        $query = (new Query())
            ->select(['from_id', 'to_id', 'from_title' => 'tsf.title', 'to_title' => 'tst.title', new Expression('GROUP_CONCAT(DISTINCT tr.type) as types')])
            ->from('sitemap_transport_intercity sti')
            ->leftJoin('transport_stop tsf', 'tsf.id = sti.from_id')
            ->leftJoin('transport_stop tst', 'tst.id = sti.to_id')
            ->leftJoin('transport_schedule_stop f', 'f.stop_id = sti.from_id')
            ->leftJoin('transport_schedule_stop t', 't.stop_id = sti.to_id')
            ->leftJoin('transport_schedule ts', 'ts.id = f.schedule_id')
            ->leftJoin('transport_route tr', 'tr.id = ts.route_id')
            ->andWhere(['sitemap_id' => $this->sitemap->id, 'tsf.is_deleted' => 0, 'tst.is_deleted' => 0])
            ->andWhere('`f`.`schedule_id` = `t`.`schedule_id`')
            ->andWhere('`f`.`order` < `t`.`order`')
            ->groupBy(['from_id', 'to_id']);
        $links = [];
        foreach ($query->batch() as $batch) {
            foreach ($batch as $link) {
                $types = explode(',', $link['types']);
                if (in_array((string)TransportRoute::TYPE_RAILWAY, $types)) {
                    if (count($types) > 1) {
                        // train and bus link
                        $trainLinks = $this->getTrainBusLinks($link, 'train', $urlManager);
                        $busLinks = $this->getTrainBusLinks($link, 'bus', $urlManager);
                        $links[$trainLinks['lv']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                        $links[$trainLinks['ru']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                        $links[$trainLinks['en']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                        $links[$busLinks['lv']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                        $links[$busLinks['ru']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                        $links[$busLinks['en']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                    } else {
                        // train link
                        $trainLinks = $this->getTrainBusLinks($link, 'train', $urlManager);
                        $links[$trainLinks['lv']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                        $links[$trainLinks['ru']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                        $links[$trainLinks['en']] = ['lv' => $trainLinks['lv'], 'ru' => $trainLinks['ru'], 'en' => $trainLinks['en']];
                    }
                } else {
                    // bus link
                    $busLinks = $this->getTrainBusLinks($link, 'bus', $urlManager);
                    $links[$busLinks['lv']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                    $links[$busLinks['ru']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                    $links[$busLinks['en']] = ['lv' => $busLinks['lv'], 'ru' => $busLinks['ru'], 'en' => $busLinks['en']];
                }
            }
        }
        return $links;
    }

    /**
     * @param $link
     * @param $type
     * @param $urlManager UrlManager
     * @return array|bool
     */
    public function getTrainBusLinks($link, $type, $urlManager)
    {
        if ($type == 'bus' || $type == 'train') {
            $paramsLv = [$type == 'bus' ? '/transport/bus/view' : '/transport/train/view', 'from' => TransportTrainBusStop::buildId(TransportTrainBusStop::TYPE_STANDARD, $link['from_id']), 'fromSlug' => UrlHelper::toSlug($link['from_title']), 'to' => TransportTrainBusStop::buildId(TransportTrainBusStop::TYPE_STANDARD, $link['to_id']), 'toSlug' => UrlHelper::toSlug($link['to_title']), 'lang' => 'lv'];
            $paramsRu = array_merge($paramsLv, ['lang' => 'ru']);
            $paramsEn = array_merge($paramsLv, ['lang' => 'en']);
            return ['lv' => $urlManager->createAbsoluteUrl($paramsLv, 'https'), 'en' => $urlManager->createAbsoluteUrl($paramsEn, 'https'), 'ru' => $urlManager->createAbsoluteUrl($paramsRu, 'https')];
        }
        return false;
    }
}