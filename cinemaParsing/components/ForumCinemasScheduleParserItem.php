<?php
namespace console\modules\entertainment\components\cinema;


use common\modules\entertainment\models\CinemaMovie;
use common\modules\entertainment\models\CinemaShow;
use common\modules\entertainment\models\CinemaTheatre;

class ForumCinemasScheduleParserItem extends BaseScheduleParserItem
{
    public $xml;

    public function getStart()
    {
        if(is_array($this->xml)){
            return $this->xml['dttmShowStart'];
        }
        else {
            return $this->xml->dttmShowStart->__toString();
        }
    }
    public function getPresentationMethod()
    {
        if(is_array($this->xml)){
            if(is_array($this->xml['PresentationMethodAndLanguage'])){
                return '';
            }
            else {
                return $this->xml['PresentationMethodAndLanguage'];
            }
        }
        else {
            return $this->xml->PresentationMethodAndLanguage->__toString();
        }
    }
    public function getType()
    {
        if(is_array($this->xml)){
            if (is_array($this->xml['PresentationMethod'])){
                $method = '';
            }
            else{
                $method = $this->xml['PresentationMethod'];
            }
        }
        else {
            $method = $this->xml->PresentationMethod->__toString();
        }
        if ($method == '3D'){
            return CinemaShow::TYPE_3D;
        }
        else{
            return CinemaShow::TYPE_NORMAL;
        }
    }
    public function getMovieId()
    {
        if(is_array($this->xml)){
            $originalTitle = $this->xml['OriginalTitle'];
        }
        else {
            $originalTitle = $this->xml->OriginalTitle->__toString();
        }
        if (array_key_exists($originalTitle, static::$movies)) {
            return static::$movies[$originalTitle];
        }
        $movie = CinemaMovie::find()->andWhere(['original_title' => $originalTitle])->one();
        static::$movies[$originalTitle] = $movie->id;

        return $movie->id;
    }
    public function getTheatreId()
    {
        $theatre = CinemaTheatre::find()->andWhere(['name' => 'Kino Citadele'])->one();
        return $theatre->id;
    }
    public function getPriority()
    {
        return CinemaTheatre::PRIORITY_FORUMCINEMAS;
    }
}