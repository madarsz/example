<?php
namespace console\modules\entertainment\components\cinema;

use \Exception;
use yii\httpclient\Client;

class CinamonScheduleParser extends BaseScheduleParser
{
    const CINAMON_SCH_URL = 'https://cinamonkino.lv/xml/';

    public function getItems()
    {
        $items = [];
        $url = static::CINAMON_SCH_URL;
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        if (!$response->isOk) {
            throw new Exception('Invalid response from '.$url);
        }
        $data = $this->parseXml($response);
        foreach ($data['Shows']['Show'] as $show) {
            $item = new CinamonScheduleParserItem();
            $item->xml = $show;
            $items[] = $item;
        }
        return $items;
    }
}