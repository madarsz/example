<?php
namespace console\modules\entertainment\components\cinema;

use \Exception;
use yii\httpclient\Client;

class ForumCinemasMovieParser extends BaseMovieParser
{
    const FORUMCINEMAS_MOVIES_URL = 'http://www.forumcinemas.lv/xml/Events/';

    public function getItems()
    {
        $items = [];
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl(static::FORUMCINEMAS_MOVIES_URL)
            ->send();
        if (!$response->isOk) {
            throw new Exception('Invalid response from '.static::FORUMCINEMAS_MOVIES_URL);
        }
        foreach ($this->parseXml($response) as $obj) {
            foreach ($obj as $movie) {
                $item = new ForumCinemasMovieParserItem();
                $item->xml = $movie;
                $items[] = $item;
            }
        }
        return $items;
    }
}