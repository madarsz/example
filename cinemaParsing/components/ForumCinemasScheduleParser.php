<?php
namespace console\modules\entertainment\components\cinema;

use mgcode\helpers\TimeHelper;
use \Exception;
use yii\httpclient\Client;

class ForumCinemasScheduleParser extends BaseScheduleParser
{
    const FORUMCINEMAS_SCH_URL = 'http://www.forumcinemas.lv/xml/Schedule/?dt='; // format for dt=30.09.2016, endpoint stores information for 6 days ahead

    public function getItems()
    {
        $items = [];
        $today = TimeHelper::getDate();
        for ($i = 0; $i < 7; $i++) {
            if ($i == 0) {
                $date = $today;
            } else {
                $date = date('d.m.Y', strtotime($today.' +'.$i.' days'));
            }
            $url = static::FORUMCINEMAS_SCH_URL.$date;
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl($url)
                ->send();
            if (!$response->isOk) {
                throw new Exception('Invalid response from '.$url);
            }
            $data = $this->parseXml($response);
            if (key_exists('Show', $data['Shows'])) {
                foreach ($data['Shows']['Show'] as $show) {
                    if (is_object($show)) {
                        $item = new ForumCinemasScheduleParserItem();
                        $item->xml = $show;
                        $items[] = $item;
                    } // Response contains only 1 show
                    else {
                        $item = new ForumCinemasScheduleParserItem();
                        $item->xml = $data['Shows']['Show'];
                        $items[] = $item;
                        break;
                    }
                }
            }
        }
        return $items;
    }
}