<?php
namespace console\modules\entertainment\components\cinema;


use common\modules\entertainment\models\CinemaTheatre;

class ForumCinemasMovieParserItem extends BaseMovieParserItem
{
    public $xml;

    public function getTitle()
    {
        return $this->xml->Title->__toString();
    }

    public function getOriginalTitle()
    {
        return $this->xml->OriginalTitle->__toString();
    }

    public function getLength()
    {
        return (int)$this->xml->LengthInMinutes;
    }

    public function getYear()
    {
        return (int)$this->xml->ProductionYear;
    }

    public function getPremiere()
    {
        $phpdate = strtotime($this->xml->dtLocalRelease->__toString());
        $mysqldate = date('Y-m-d', $phpdate);
        return $mysqldate;
    }

    public function getSynopsis()
    {
        return $this->xml->Synopsis->__toString();
    }

    public function getShortSynopsis()
    {
        return $this->xml->ShortSynopsis->__toString();
    }

    public function getImage()
    {
        return $this->xml->Images->EventLargeImagePortrait->__toString();
    }

    public function getActors()
    {
        $actors = [];
        foreach ($this->xml->Cast->Actor as $actor) {
            $actors[] = ['firstName' => $actor->FirstName->__toString(), 'lastName' => $actor->LastName->__toString()];
        }
        return $actors;
    }

    public function getDirectors()
    {
        $directors = [];
        foreach ($this->xml->Directors->Director as $director) {
            $directors[] = ['firstName' => $director->FirstName->__toString(), 'lastName' => $director->LastName->__toString()];
        }
        return $directors;
    }

    public function getGenres()
    {
        $genres = explode(',', $this->xml->Genres->__toString());
        $parsed = [];
        foreach ($genres as $genre){
            $genre = str_replace(' filma', '', $genre);
            $parsed[] = trim($genre);
        }
        return $parsed;
    }

    public function getYoutubeVideos()
    {
        $videos = [];
        foreach ($this->xml->Videos->EventVideo as $event){
            if ($event->MediaResourceFormat->__toString() == 'YouTubeVideo'){
                $videos[] = $event->Location->__toString();
            }
        }
        return $videos;
    }

    public function getPriority()
    {
        return CinemaTheatre::PRIORITY_FORUMCINEMAS;
    }
}