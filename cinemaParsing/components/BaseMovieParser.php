<?php
namespace console\modules\entertainment\components\cinema;

use yii\base\Component;
use yii\httpclient\Response;

abstract class BaseMovieParser extends Component
{
    use XmlParserTrait;

    /**
     * This function should return an array of movie items extended from BaseMovieParserItem
     * @return BaseMovieParserItem[]
     */
    abstract function getItems();

    /**
     * Function performs save for all the items it receives from getItems()
     */
    public function run(){
        /** @var BaseMovieParserItem[] $items */
        $items = $this->getItems();
        foreach ($items as $item){
            $item->save();
        }
    }
}