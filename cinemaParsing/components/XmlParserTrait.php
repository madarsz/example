<?php
namespace console\modules\entertainment\components\cinema;

use yii\httpclient\Response;

trait XmlParserTrait
{
    /**
     * Back compatibility fix.
     * New httpclient library breaks code.
     * @return array
     */
    protected function parseXml(Response $response)
    {
        $contentType = $response->getHeaders()->get('content-type', '');
        if (preg_match('/charset=(.*)/i', $contentType, $matches)) {
            $encoding = $matches[1];
        } else {
            $encoding = 'UTF-8';
        }
        $dom = new \DOMDocument('1.0', $encoding);
        $dom->loadXML($response->getContent(), LIBXML_NOCDATA);
        return $this->_convertXmlToArray(simplexml_import_dom($dom->documentElement));
    }

    /**
     * Converts XML document to array.
     * @param string|\SimpleXMLElement $xml xml to process.
     * @return array XML array representation.
     */
    private function _convertXmlToArray($xml)
    {
        if (!is_object($xml)) {
            $xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        }
        $result = (array) $xml;
        foreach ($result as $key => $value) {
            if (is_object($value)) {
                $result[$key] = $this->_convertXmlToArray($value);
            }
        }
        return $result;
    }
}