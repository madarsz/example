<?php
namespace console\modules\entertainment\components\cinema;

use mgcode\commandLogger\LoggingTrait;
use yii\base\Component;
use \Exception;

abstract class BaseScheduleParser extends Component
{
    use XmlParserTrait, LoggingTrait;

    /**
     * Function should return an array of schedule items extended from BaseScheduleParserItem
     * @return BaseScheduleParserItem[]
     */
    abstract function getItems();

    /**
     * Function saves all BaseScheduleParserItem objects it receives from getItems()
     */
    public function run()
    {
        /** @var BaseScheduleParserItem[] $items */
        $items = $this->getItems();

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $data = [];
            $dataForUpdate = [];
            $theatreId = null;
            foreach ($items as $item) {
                $data[] = $item->getData();
                $dataForUpdate[$item->getMovieId()][] = $item->getStart();
                $theatreId = $item->getTheatreId();
            }
            $sql = \Yii::$app->db->createCommand()->batchInsert('cinema_show', ['start', 'presentation_method', 'type', 'movie_id', 'theatre_id', 'updated'], $data)->rawSql;
            if ($sql) {
                $sql .= ' ON DUPLICATE KEY UPDATE `start` = VALUES(`start`), `presentation_method` = VALUES(`presentation_method`), `type` = VALUES(`type`), `movie_id` = VALUES(`movie_id`), `theatre_id` = VALUES(`theatre_id`), `updated` = VALUES(`updated`), `is_deleted` = 0';
                \Yii::$app->db->createCommand($sql)->execute();
            }
            if ($theatreId) {
                $whereQueries = [];
                $sql = 'UPDATE cinema_show SET `is_deleted` = 1 WHERE `theatre_id` = '.$theatreId.' and (';
                $time = date("Y-m-d H:i:s");
                foreach ($dataForUpdate as $id => $starts) {
                    $whereQueries[] = "(`movie_id` = ".$id." and `start` >= '".$time."' and `start` not in ('".implode("', '", $starts)."'))";
                }
                $sql .= implode(' OR ', $whereQueries).')';
                \Yii::$app->db->createCommand($sql)->execute();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }
}