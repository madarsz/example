<?php
namespace console\modules\entertainment\components\cinema;

use common\modules\entertainment\models\CinemaGenre;
use common\modules\entertainment\models\CinemaMovie;
use common\modules\entertainment\models\CinemaMovieGenre;
use common\modules\entertainment\models\CinemaShow;
use common\modules\entertainment\models\CinemaTheatre;

class CinamonScheduleParserItem extends BaseScheduleParserItem
{
    public $xml;

    public function getStart()
    {
        return $this->xml->dttmShowStart->__toString();
    }

    public function getPresentationMethod()
    {
        return $this->xml->PresentationMethodAndLanguage->__toString();
    }

    public function getType()
    {
        if (strpos($this->xml->PresentationMethod->__toString(), '3D') !== false) {
            return CinemaShow::TYPE_3D;
        } else {
            return CinemaShow::TYPE_NORMAL;
        }
    }

    public function getMovieId()
    {
        $originalTitle = str_replace(' 3D', '', $this->xml->OriginalTitle->__toString());
        $title = str_replace(' 3D', '', $this->xml->Title->__toString());
        if (!$title && !$originalTitle) {
            return null;
        }
        if (!$title) {
            $title = $originalTitle;
        }
        if (!$originalTitle) {
            $originalTitle = $title;
        }

        if (array_key_exists($originalTitle, static::$movies)) {
            return static::$movies[$originalTitle];
        }
        $movie = CinemaMovie::find()->andWhere(['original_title' => $originalTitle])->one();
        if(!$movie) {
            $movie = new CinemaMovie();
            $movie->title = $title;
            $movie->original_title = $originalTitle;
            $movie->length = $this->xml->LengthInMinutes->__toString();
            $movie->image_id = \Yii::$app->image->saveFromUrl($this->xml->Images->EventLargeImagePortrait->__toString())->id;
            $movie->priority = $this->getPriority();
            $movie->saveOrFail();
            $movie->calculateSimilar();
            $this->registerGenres($movie->id, explode(',', $this->xml->Genres->__toString()));
        }
        static::$movies[$originalTitle] = $movie->id;

        return $movie->id;
    }

    public function getTheatreId()
    {
        $theatre = CinemaTheatre::find()->andWhere(['name' => 'Cinamon Riga'])->one();
        return $theatre->id;
    }

    public function getPriority()
    {
        return CinemaTheatre::PRIORITY_CINAMON;
    }

    public function getOriginalTitle()
    {
        return $this->xml->OriginalTitle->__toString();
    }
}