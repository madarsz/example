<?php
namespace console\modules\entertainment\components\cinema;


use common\modules\entertainment\models\CinemaGenre;
use common\modules\entertainment\models\CinemaMovieGenre;
use common\modules\entertainment\models\CinemaShow;
use yii\base\Component;
use yii\db\Expression;

/**
 * Class BaseScheduleParserItem
 * @package console\modules\entertainment\components\cinema
 *
 * Base class for parsing schedule of cinemas.
 * Each abstract function should return the appropriate attribute of the scheduled show.
 */
abstract class BaseScheduleParserItem extends Component
{
    abstract function getStart();
    abstract function getPresentationMethod();
    abstract function getType();
    abstract function getMovieId();
    abstract function getTheatreId();
    abstract function getPriority();

    static $movies = [];

    public function getData()
    {
        $data = [];
        $data[] = $this->getStart();
        $data[] = $this->getPresentationMethod();
        $data[] = $this->getType();
        $data[] = $this->getMovieId();
        $data[] = $this->getTheatreId();
        $data[] = new Expression('NOW()');

        return $data;
    }

    /**
     * Registers genres for a new movie.
     * Function used for theatres that have api endpoint only for schedule
     * @param $movieId
     * @param $genres
     */
    protected function registerGenres($movieId, $genres)
    {
        foreach ($genres as $genre){
            // Performs parsing of similar genres
            $genre = $this->parseGenre($genre);
            // Creates genre if needed
            $genreObj = $this->getGenre($genre);
            // Links genre to movie
            $movieGenre = CinemaMovieGenre::find()->andWhere(['movie_id' => $movieId, 'genre_id' => $genreObj->id])->one();
            if (!$movieGenre){
                $movieGenre = new CinemaMovieGenre();
                $movieGenre->genre_id = $genreObj->id;
                $movieGenre->movie_id = $movieId;
                $movieGenre->save();
            }
        }
    }

    /**
     * @param $genre
     * @return CinemaGenre
     */
    protected function getGenre($genre)
    {
        $genreObj = CinemaGenre::find()->andWhere(['name' => $genre])->one();
        if (!$genreObj){
            $genreObj = new CinemaGenre();
            $genreObj->name = $genre;
            $genreObj->saveOrFail();
        }
        return $genreObj;
    }

    /**
     * Performs parsing of similar genres
     * @param $genre
     * @return string
     */
    protected function parseGenre($genre)
    {
        $genre = str_replace(' filma', '', $genre);
        $genre = trim($genre);
        if ($genre == 'Animācijas'){
            $genre = 'Animācija';
        }
        return $genre;
    }
}