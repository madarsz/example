<?php
namespace console\modules\entertainment\components\cinema;

use common\modules\entertainment\models\CinemaActor;
use common\modules\entertainment\models\CinemaDirector;
use common\modules\entertainment\models\CinemaGenre;
use common\modules\entertainment\models\CinemaMovie;
use common\modules\entertainment\models\CinemaMovieActor;
use common\modules\entertainment\models\CinemaMovieDirector;
use common\modules\entertainment\models\CinemaMovieGenre;
use common\modules\entertainment\models\CinemaVideo;
use yii\base\Component;
use yii\db\Query;

/**
 * Class BaseMovieParserItem
 * @package console\modules\entertainment\components\cinema
 *
 * Base class for parsing movies.
 * Each abstract function should return the appropriate attribute of the movie
 */
abstract class BaseMovieParserItem extends Component
{
    abstract function getTitle();
    abstract function getOriginalTitle();
    abstract function getLength();
    abstract function getYear();
    abstract function getPremiere();
    abstract function getSynopsis();
    abstract function getShortSynopsis();
    abstract function getImage();
    abstract function getActors();
    abstract function getDirectors();
    abstract function getGenres();
    abstract function getYoutubeVideos();
    abstract function getPriority();

    public function save(){
        $time = strtotime("-1 year", time());
        $originalTitle = $this->getOriginalTitle();
        if (!$originalTitle){
            if (!$this->getTitle()){
                return;
            } else {
                $originalTitle = $this->getTitle();
            }
        }
        $movie = CinemaMovie::find()->andWhere(['original_title' => $originalTitle])->andWhere(['>=', 'created', $time])->one();
        if ($movie){
            // Update movie only if the provider has lower priority number (means more trusted)
            if ($movie->priority <= $this->getPriority()){
                return;
            }
        }
        else {
            $movie = new CinemaMovie();
        }
        $movie->title = $this->getTitle();
        $movie->original_title = $this->getOriginalTitle();
        if (!$movie->title && !$movie->original_title){
            return;
        }
        if (!$movie->title){
            $movie->title = $movie->original_title;
        }
        if (!$movie->original_title){
            $movie->original_title = $movie->title;
        }
        $movie->length = $this->getLength();
        $movie->year = $this->getYear();
        $movie->premiere = $this->getPremiere();
        $movie->synopsis = $this->getSynopsis();
        $movie->short_synopsis = $this->getShortSynopsis();
        $movie->priority = $this->getPriority();
        if (!$movie->image_id){
            $movie->image_id = \Yii::$app->image->saveFromUrl($this->getImage())->id;
        }
        if ($movie->isNewRecord || $movie->isAnyAttributeChanged(['title', 'original_title', 'length', 'premiere', 'synopsis', 'short_synopsis', 'priority'])) {
            $movie->saveOrFail();
            $movie->calculateSimilar();
        }
        foreach ($this->getActors() as $actor){
            $actorObj = CinemaActor::find()->andWhere(['first_name' => $actor['firstName'], 'last_name' => $actor['lastName']])->one();
            if (!$actorObj){
                $actorObj = new CinemaActor();
                $actorObj->first_name = $actor['firstName'];
                $actorObj->last_name = $actor['lastName'];
                $actorObj->saveOrFail();
            }
            $movieActor = CinemaMovieActor::find()->andWhere(['movie_id' => $movie->id, 'actor_id' => $actorObj->id])->one();
            if (!$movieActor){
                $movieActor = new CinemaMovieActor();
                $movieActor->actor_id = $actorObj->id;
                $movieActor->movie_id = $movie->id;
                $movieActor->save();
            }
        }
        foreach ($this->getDirectors() as $director){
            $directorObj = CinemaDirector::find()->andWhere(['first_name' => $director['firstName'], 'last_name' => $director['lastName']])->one();
            if (!$directorObj){
                $directorObj = new CinemaDirector();
                $directorObj->first_name = $director['firstName'];
                $directorObj->last_name = $director['lastName'];
                $directorObj->saveOrFail();
            }
            $movieDirector = CinemaMovieDirector::find()->andWhere(['movie_id' => $movie->id, 'director_id' => $directorObj->id])->one();
            if (!$movieDirector){
                $movieDirector = new CinemaMovieDirector();
                $movieDirector->director_id = $directorObj->id;
                $movieDirector->movie_id = $movie->id;
                $movieDirector->save();
            }
        }
        $genresId = [];
        foreach ($this->getGenres() as $genre){
            $genreObj = CinemaGenre::find()->andWhere(['name' => $genre])->one();
            if (!$genreObj){
                $genreObj = new CinemaGenre();
                $genreObj->name = $genre;
                $genreObj->saveOrFail();
            }
            $genresId[] = $genreObj->id;
            $movieGenre = CinemaMovieGenre::find()->andWhere(['movie_id' => $movie->id, 'genre_id' => $genreObj->id])->one();
            if (!$movieGenre){
                $movieGenre = new CinemaMovieGenre();
                $movieGenre->genre_id = $genreObj->id;
                $movieGenre->movie_id = $movie->id;
                $movieGenre->save();
            }
        }
        // When overwriting movie that has been received from higher priority data provider,
        // deleting genres that aren't provided by it.
        \Yii::$app->db->createCommand()->delete('cinema_movie_genre',
            [
                'AND',
                ['movie_id' => $movie->id],
                ['not in', 'genre_id', $genresId]
            ])->execute();

        foreach ($this->getYoutubeVideos() as $video){
            $link = 'https://www.youtube.com/embed/'.$video;
            $videoObj = CinemaVideo::find()->andWhere(['youtube' => $link, 'movie_id' => $movie->id])->one();
            if (!$videoObj){
                $videoObj = new CinemaVideo();
                $videoObj->movie_id = $movie->id;
                $videoObj->youtube = $link;
                $videoObj->saveOrFail();
            }
        }
    }
}