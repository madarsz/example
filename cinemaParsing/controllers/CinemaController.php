<?php
namespace console\modules\entertainment\controllers;

use common\modules\entertainment\components\CinemaComponent;
use common\modules\entertainment\models\CinemaMovie;
use common\modules\entertainment\models\CinemaMovieElastic;
use console\modules\entertainment\components\cinema\CinamonScheduleParser;
use console\modules\entertainment\components\cinema\ForumCinemasMovieParser;
use console\modules\entertainment\components\cinema\ForumCinemasScheduleParser;
use console\modules\entertainment\components\cinema\KinoGaismaScheduleParser;
use console\modules\entertainment\components\cinema\MultikinoScheduleParser;
use console\modules\entertainment\components\cinema\SilverScreenDaugavpilsScheduleParser;
use console\modules\entertainment\components\cinema\SilverScreenMovieParser;
use console\modules\entertainment\components\cinema\SilverScreenRezekneScheduleParser;
use mgcode\commandLogger\LoggingTrait;
use \Exception;
use yii\console\Controller;

class CinemaController extends Controller
{
    use LoggingTrait;

    /** @var bool Deletes and creates new index */
    public $reIndex = false;

    public function actionIndex()
    {
        $this->msg("Starting full synchronization.");
        $this->actionForumcinemasMovies();
        $this->actionSilverscreenMovies();
        $this->actionForumcinemasSchedule();
        $this->actionSilverscreenRezekneSchedule();
        $this->actionSilverscreenDaugavpilsSchedule();
        $this->actionCinamonSchedule();
        $this->actionKinogaismaSchedule();
        $this->actionMultikinoSchedule();
        $this->actionFillElastic();
        $this->msg("Finished full synchronization.");
    }

    public function actionFillElastic()
    {
        if ($this->mustCreateIndex()) {
            $this->msg('Creating index...');
            CinemaMovieElastic::deleteIndex();
            CinemaMovieElastic::createIndex();
            $this->msg('done');
        }

        $this->msg('Importing movies into ElasticSearch...');
        CinemaComponent::updateElastic();
        $this->msg('done');
    }

    public function actionForumcinemasMovies()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $fp = new ForumCinemasMovieParser();
            $fp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionSilverscreenMovies()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $sp = new SilverScreenMovieParser();
            $sp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionForumcinemasSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $fsp = new ForumCinemasScheduleParser();
            $fsp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionSilverscreenRezekneSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $srsp = new SilverScreenRezekneScheduleParser();
            $srsp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionSilverscreenDaugavpilsSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $srsp = new SilverScreenDaugavpilsScheduleParser();
            $srsp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionCinamonSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $csp = new CinamonScheduleParser();
            $csp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionKinogaismaSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $ksp = new KinoGaismaScheduleParser();
            $ksp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionMultikinoSchedule()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $msp = new MultikinoScheduleParser();
            $msp->run();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->logException($e);
        }
    }

    public function actionCalculateAllPairs()
    {
        $this->msg("Starting full similarity calculation.");
        $find = CinemaMovie::find()
            ->with(['cinemaMovieActors', 'cinemaMovieDirectors', 'cinemaMovieGenres']);

        /** @var CinemaMovie $movie */
        foreach($find->each() as $movie) {
            $movie->calculateSimilar();
        }
        $this->msg("Finished full similarity calculation.");
    }

    /**
     * Whether index must be created.
     * @return bool
     */
    protected function mustCreateIndex()
    {
        if ($this->reIndex) {
            return true;
        }

        $db = \Yii::$app->elasticsearch;

        $response = $db->head([CinemaMovieElastic::index(), CinemaMovieElastic::type()]);
        if (!$response) {
            return true;
        }
        return false;
    }

    /** @inheritdoc */
    public function options($actionID)
    {
        $options = parent::options($actionID);

        switch ($actionID) {
            case 'fill-elastic':
                $options = array_merge($options, ['reIndex']);
                break;
        }

        return $options;
    }

    /** @inheritdoc */
    public function optionAliases()
    {
        return ['ri' => 'reIndex'];
    }
}
