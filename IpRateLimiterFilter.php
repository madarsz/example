<?php
namespace api\components;

use mgcode\helpers\RequestHelper;
use yii\base\ActionFilter;
use yii\db\Query;
use yii\web\TooManyRequestsHttpException;
use yii\db\Connection;
use yii\base\InvalidConfigException;
use yii\di\Instance;

/**
 * This filter adds limits to amount of allowed requests per action per ip.
 * Filter checks requests ip and also forwarded ip if present.
 * If limits are exceeded then exception is thrown with code 429 (meaning "Too Many Requests")
 *
 * Usage example
 * ~~~
 * public function behaviors()
 * {
 *     return [
 *         'ipRateLimiter' => [
 *             'class' => \api\components\IpRateLimiterFilter::className(),
 *             'limits' => [
 *                  [100, 3600],
 *                  [500, 86400]
 *              ],
 *         ],
 *     ];
 * }
 * ~~~
 */
class IpRateLimiterFilter extends ActionFilter
{
    /**
     * @var array
     * Array of arrays that defines applied limits
     * Each element of this array should be an array that consists of two elements
     * First one being request count and the second interval in seconds
     *
     * Example:
     * [
     *      [100, 3600],
     *      [500, 86400]
     * ]
     * This example defines two limits:
     *      * 100 requests per hour (3600 seconds)
     *      * 500 requests per day (86400 seconds)
     */
    public $limits;

    /**
     * @var string
     * Table name used for logging requests in database
     * Must contain fields: ip, action, timestamp
     */
    public $apiTableName = 'api_request';

    /**
     * @var Connection|array|string the DB connection object or the application component ID of the DB connection.
     * After the IpRateLimiterFilter object is created, if you want to change this property, you should only assign it
     * with a DB connection object.
     * Starting from version 2.0.2, this can also be a configuration array for creating the object.
     */
    public $db = 'db';

    /**
     * Initializes the IpRateLimiterFilter component.
     * This method will initialize the [[db]] property to make sure it refers to a valid DB connection.
     * @throws InvalidConfigException if [[db]] is invalid.
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::className());
    }

    /**
     * @param \yii\base\InlineAction $action
     * @return boolean whether the action should continue to be executed.
     * @throws TooManyRequestsHttpException
     */
    public function beforeAction($action)
    {
        $userIp = \Yii::$app->request->userIP;
        $forwardedIp = RequestHelper::getUserIpForwarded();
        $actionUniqueId = $action->getUniqueId();
        foreach ($this->limits as $limit) {
            if ($userIp) {
                $this->checkIp($userIp, $actionUniqueId, $limit);
            }
            if ($forwardedIp) {
                $this->checkIp($forwardedIp, $actionUniqueId, $limit);
            }
        }
        return true;
    }

    /**
     * This method is invoked right after an action is executed.
     * @param \yii\base\InlineAction $action the action just executed.
     * @param mixed $result the action execution result
     * @return mixed the processed action result.
     */
    public function afterAction($action, $result)
    {
        $userIp = \Yii::$app->request->userIP;
        $forwardedIp = RequestHelper::getUserIpForwarded();
        $actionUniqueId = $action->getUniqueId();
        if ($userIp) {
            $this->logRequest($userIp, $actionUniqueId);
        }
        if ($forwardedIp) {
            $this->logRequest($forwardedIp, $actionUniqueId);
        }
        return $result;
    }

    protected function checkIp($ip, $action, $limit)
    {
        $ip = RequestHelper::ipToUnsignedInt($ip);
        $from = time() - $limit[1];
        $count = (new Query())
            ->from($this->apiTableName)
            ->andWhere(['ip' => $ip, 'action' => $action])
            ->andWhere(['>', 'timestamp', $from])
            ->count('id', $this->db);
        if ($count >= $limit[0]) {
            throw new TooManyRequestsHttpException();
        }
        return true;
    }

    protected function logRequest($ip, $action)
    {
        return $this->db->createCommand()->insert($this->apiTableName, [
            'ip' => RequestHelper::ipToUnsignedInt($ip),
            'action' => $action,
            'timestamp' => time(),
        ])->execute();
    }
}