<?php
namespace console\modules\vzd\controllers;

use common\modules\vzd\models\Vzd;
use console\modules\vzd\actions\GenerateGeometryAction;
use console\modules\vzd\actions\VzdApartmentAction;
use console\modules\vzd\actions\VzdApartmentCoordinateAction;
use console\modules\vzd\actions\VzdCountyAction;
use console\modules\vzd\actions\VzdDistrictAction;
use console\modules\vzd\actions\VzdFullNamesAction;
use console\modules\vzd\actions\VzdHouseAction;
use console\modules\vzd\actions\VzdHouseCoordinateAction;
use console\modules\vzd\actions\VzdParishAction;
use console\modules\vzd\actions\VzdStreetAction;
use console\modules\vzd\actions\VzdTownAction;
use console\modules\vzd\actions\VzdVillageAction;
use mgcode\commandLogger\LoggingTrait;
use mgcode\helpers\ArrayHelper;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Console;

class SyncController extends Controller
{
    use LoggingTrait;

    /** @var bool Generates geometries for all objects */
    public $all = false;

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'district' => VzdDistrictAction::className(),
            'county' => VzdCountyAction::className(),
            'parish' => VzdParishAction::className(),
            'town' => VzdTownAction::className(),
            'village' => VzdVillageAction::className(),
            'street' => VzdStreetAction::className(),
            'house' => VzdHouseAction::className(),
            'apartment' => VzdApartmentAction::className(),
            'house-coordinate' => VzdHouseCoordinateAction::className(),
            'apartment-coordinate' => VzdApartmentCoordinateAction::className(),
            'full-names' => VzdFullNamesAction::className(),
            'generate-geometry' => GenerateGeometryAction::className(),
        ]);
    }

    /**
     * Runs all Vzd actions in correct order.
     */
    public function actionIndex()
    {
        $this->msg('Syncing Vzd data');
        gc_enable();
        $this->runAction('district');
        $this->runAction('county');
        $this->runAction('parish');
        $this->runAction('town');
        $this->runAction('village');
        $this->runAction('street');
        $this->runAction('house');
        $this->runAction('apartment');
        $this->runAction('house-coordinate');
        $this->runAction('apartment-coordinate');
        $this->msg('Finished syncing Vzd data');
    }

    public function actionGenerateGeometries()
    {
        $this->msg("Starting geometry generation");
        $types = [
            Vzd::TYPE_CITY,
            Vzd::TYPE_PARISH,
            Vzd::TYPE_VILLAGE,
            Vzd::TYPE_COUNTY,
            Vzd::TYPE_CITY_DISTRICT,
        ];
        $query = (new Query())
            ->select(['v.code'])
            ->from('vzd v')
            ->andWhere(['v.is_deleted' => 0, 'v.status' => Vzd::STATUS_EKS, 'v.type' => $types]);
        if ($this->all) {
            $query->andWhere(['v.osm_id' => null]);
        } else {
            $query->leftJoin('vzd_geometry g', 'g.code = v.code')->andWhere(['g.geometry' => null]);
        }
        $total = $query->count();
        $count = 0;
        Console::startProgress(0, $total);
        foreach ($query->batch() as $objects) {
            foreach ($objects as $object) {
                try {
                    $this->runAction('generate-geometry', [$object['code']]);
                } catch (\Exception $e) {
                    $this->logException($e);
                }
                $count++;
                Console::updateProgress($count, $total);
            }
        }
        $this->msg("Finished geometry generation");
    }

    /** @inheritdoc */
    public function options($actionID)
    {
        $options = parent::options($actionID);

        switch ($actionID) {
            case 'generate-geometries':
                $options = array_merge($options, ['all']);
                break;
        }
        return $options;
    }
}