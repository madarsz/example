<?php

namespace console\modules\vzd\actions;

use console\modules\vzd\actions\base\VzdBaseLargeAction;

class VzdApartmentAction extends VzdBaseLargeAction
{
    /**
     * Syncs Vzd Apartments
     */
    public function run()
    {
        $links = $this->getLinks();
        $this->msg('Syncing Apartments');
        $this->syncApartment($links->AddressObjectDataResponse->AddressObjectDataResult->ApartmentDataLink);
        $this->msg('Finished syncing Apartments');
    }

    /**
     * This method syncs data from VZD xml files: AW_DZIV
     * @param $link
     */
    protected function syncApartment($link)
    {
        $this->getDataLarge($link, null);
    }
}