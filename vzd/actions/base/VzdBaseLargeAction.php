<?php

namespace console\modules\vzd\actions\base;

use mgcode\helpers\TimeHelper;

/**
 * Base class used for endpoints with large volume of data
 * Class VzdBaseLargeAction
 * @package console\modules\vzd\actions\base
 */
abstract class VzdBaseLargeAction extends VzdBaseAction
{
    /**
     * @param $url
     * @param |null $infoFunction
     * @param null $infoMessage
     */
    protected function getDataLarge($url, $infoFunction, $infoMessage = null)
    {
        $reader = new \XMLReader();
        $reader->open($url);
        $i = 0;
        $this->msg('Processing data');
        $data['items'] = [];
        $data['prepared'] = TimeHelper::getTime();
        while($reader->read())
        {
            if($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'n1:AddressObjectData')
            {
                $xml = simplexml_load_string($reader->readOuterXml());
                $xml->registerXPathNamespace('n1', 'http://ivis.eps.gov.lv/XMLSchemas/100007/AddressRegistry/v1-0');
                $part = $xml->xpath("//n1:AddressObjectData");
                $obj = $part[0]->children("n1",true);
                $data['items'][] = [
                    'code' => isset($obj->Code) ? $obj->Code->__toString() : null,
                    'type' => isset($obj->Type) ? $obj->Type->__toString() : null,
                    'status' => isset($obj->Status) ? $obj->Status->__toString() : null,
                    'approved' => isset($obj->Approved) ? $obj->Approved->__toString() : null,
                    'approveDegree' => isset($obj->ApproveDegree) ? $obj->ApproveDegree->__toString() : null,
                    'parentCode' => isset($obj->ParentCode) ? $obj->ParentCode->__toString() : null,
                    'parentType' => isset($obj->ParentType) ? $obj->ParentType->__toString() : null,
                    'name' => isset($obj->Name) ? $obj->Name->__toString() : null,
                    'sortName' => isset($obj->SortName) ? $obj->SortName->__toString() : null,
                    'dateCreated' => isset($obj->DateCreated) ? $obj->DateCreated->__toString() : null,
                    'dateModified' => isset($obj->DateModified) ? $obj->DateModified->__toString() : null,
                    'dateDeleted' => isset($obj->DateDeleted) ? $obj->DateDeleted->__toString() : null,
                    'atuCode' => isset($obj->ATUCode) ? $obj->ATUCode->__toString() : null,
                    'villageCode' => isset($obj->VillageCode) ? $obj->VillageCode->__toString() : null,
                    'streetCode' => isset($obj->StreetCode) ? $obj->StreetCode->__toString() : null,
                    'postIndex' => isset($obj->PostIndex) ? $obj->PostIndex->__toString() : null,
                    'postOfficeCode' => isset($obj->PostOfficeCode) ? $obj->PostOfficeCode->__toString() : null,
                    'isBuild' => isset($obj->ForBuild) ? $obj->ForBuild->__toString() : null,
                ];
                $i++;
                if ($i % 500 == 0) {
                    $this->msg("{$i} records processed");
                }
                if ($i % 20000 == 0) {
                    $this->msg('Inserting 20000 records');
                    $this->msg('Inserting data objects');
                    $this->insertVzdDataObjects($data);
                    if (is_callable($infoFunction)) {
                        if ($infoMessage) {
                            $this->msg($infoMessage);
                        }
                        $infoFunction($data['items']);
                    }
                    unset($data['items']);
                    $data['items'] = [];
                    gc_collect_cycles();
                }
            }
        }
        if (!empty($data['items'])) {
            $this->msg('Inserting data objects');
            $this->insertVzdDataObjects($data);
            if (is_callable($infoFunction)) {
                if ($infoMessage) {
                    $this->msg($infoMessage);
                }
                $infoFunction($data['items']);
            }
        }
        $this->msg('Finished processing data');
    }
}