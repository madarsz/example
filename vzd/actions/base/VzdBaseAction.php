<?php

namespace console\modules\vzd\actions\base;

use common\modules\vzd\models\Vzd;
use mgcode\commandLogger\LoggingTrait;
use yii\base\Action;

abstract class VzdBaseAction extends Action
{
    use LoggingTrait;

    private $_client;

    /**
     * Makes SOAP request
     * @param $dataLevel
     * @param bool $objectCoordinates
     * @param bool $geoLink
     * @return mixed
     */
    protected function makeRequest($dataLevel, $objectCoordinates = true, $geoLink = true)
    {
        $client = $this->getClient();

        $params = [
            'AddressObjectDataRequest' => [
                'Login' => [
                    'Username' => \Yii::$app->params['vzdUser'],
                    'Password' => \Yii::$app->params['vzdPassword'],
                ],
                'AddressObjectDataParameters' => [
                    'DataLevel' => $dataLevel,
                    'ObjectCoordinates' => $objectCoordinates ? 'True' : 'False',
                    'GeoLinkData' => $geoLink ? 'True' : 'False',
                ],
                'EServiceInstance' => null,
            ]
        ];
        $result = $client->__soapCall('getAddressObjectData', [$params]);

        return $result;
    }

    /**
     * Returns client for SOAP requests
     * @return \SoapClient
     */
    protected function getClient()
    {
        if (!$this->_client) {
            ;
            $opts = [
                'http' => [
                    'user_agent' => 'PHPSoapClient'
                ]
            ];

            $context = stream_context_create($opts);
            $this->_client = new \SoapClient('https://ws.kadastrs.lv/ws_suite/addressobjectdata/wsdl', [
                'soap_version' => SOAP_1_1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'exceptions' => true,
                'stream_context' => $context,
                'trace' => 1,
                'local_cert' => '',
                'passphrase' => '',
            ]);
        }

        return $this->_client;
    }

    protected function insertVzdDataObjects($data)
    {
        $db = \Yii::$app->db;
        $sql = "INSERT INTO vzd (code, type, status, is_approved, approve_degree, parent_code, parent_type, name, sort_name, created, updated, deleted, prepared, is_deleted) VALUES";
        $prepared = $data['prepared'] ? $db->quoteValue($data['prepared']) : 'null';
        $data = $data['items'];
        foreach ($data as $obj) {
            $code = $db->quoteValue($obj['code']);
            $type = $db->quoteValue((int) $obj['type']);
            $status = Vzd::getStatuses()[$obj['status']];
            $isApproved = $obj['approved'] == 'Y' ? 1 : 0;
            $approveDegree = $db->quoteValue((int) $obj['approveDegree']);
            $parentCode = $db->quoteValue($obj['parentCode']);
            $parentType = $db->quoteValue((int) $obj['parentType']);
            $name = $db->quoteValue($this->prepareName($obj['name']));
            $sortName = $db->quoteValue($obj['sortName']);
            $created = $db->quoteValue($obj['dateCreated']);
            $updated = $db->quoteValue($obj['dateModified']);
            $deleted = $db->quoteValue($obj['dateDeleted']) ? $db->quoteValue($obj['dateDeleted']) : 'null';
            $isDeleted = isset($obj['dateDeleted']) && $obj['dateDeleted'] ? 1 : 0;
            if ($parentCode && $parentType) {
                // code, type, status, is_approved, approve_degree, parent_code, parent_type, name, sort_name, created, updated, deleted, prepared, is_deleted
                $sql .= "({$code}, {$type}, {$status}, {$isApproved}, {$approveDegree}, {$parentCode}, {$parentType}, {$name}, {$sortName}, {$created}, {$updated}, {$deleted}, {$prepared}, {$isDeleted}),";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE `status` = VALUES(`status`), `is_approved` = VALUES(`is_approved`), `approve_degree` = VALUES(`approve_degree`), `parent_code` = VALUES(`parent_code`), `parent_type` = VALUES(`parent_type`), `name` = VALUES(`name`), `sort_name` = VALUES(`sort_name`), `created` = VALUES(`created`), `updated` = VALUES(`updated`), `deleted` = VALUES(`deleted`), `is_deleted` = VALUES(`is_deleted`);';
        $db->createCommand($sql)->execute();
    }

    protected function prepareName($name)
    {
        $name = str_replace('raj.', 'rajons', $name);
        $name = str_replace('nov.', 'novads', $name);
        $name = str_replace('pag.', 'pagasts', $name);
        return $name;
    }

    /**
     * Possible $dataLevel values: Street, House, Apartment
     * @return mixed
     */
    protected function getLinks($dataLevel = 'Apartment', $objectCoordinates = false, $geoLink = false)
    {
        $result = $this->makeRequest($dataLevel, $objectCoordinates, $geoLink);
        return $result;
    }

    /**
     * Inserts prepared coordinates into database
     * @param array $coordinates
     * @throws \yii\db\Exception
     */
    protected function insertCoordinates($coordinates)
    {
        if (!$coordinates) {
            return;
        }

        $db = \Yii::$app->db;
        $sql = $db->createCommand()->batchInsert('vzd_coordinates', ['code', 'lat', 'long'], $coordinates)->getRawSql();
        $sql .= ' ON DUPLICATE KEY UPDATE `lat` = VALUES(`lat`), `long` = VALUES(`long`);';
        $db->createCommand($sql)->execute();
    }
}