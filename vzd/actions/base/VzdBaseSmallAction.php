<?php

namespace console\modules\vzd\actions\base;

use console\modules\vzd\components\XMLParser;
use yii\helpers\Console;
use yii\httpclient\Client;

/**
 * Base class used for endpoints with small volume of data
 * Class VzdBaseSmallAction
 * @package console\modules\vzd\actions\base
 */
abstract class VzdBaseSmallAction extends VzdBaseAction
{
    protected function getData($url, $listName)
    {
        $items = [];
        $prepared = null;
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        if (!$response->isOk) {
            throw new \Exception('Invalid response from '.$url);
        }

        $p = new XMLParser($response->content);
        $output = $p->getOutput();
        $data = $output['env:Envelope']['env:Body']['n1:AddressObjectDataFile']['n1:'.$listName];
        $total = count($data);
        $this->msg('Processing data');
        Console::startProgress(0, $total, $this->getMemoryUsageMsg());
        $i = 0;
        foreach ($data as $obj) {
            $items[] = [
                'code' => isset($obj['n1:Code']) ? $obj['n1:Code'] : null,
                'type' => isset($obj['n1:Type']) ? $obj['n1:Type'] : null,
                'status' => isset($obj['n1:Status']) ? $obj['n1:Status'] : null,
                'approved' => isset($obj['n1:Approved']) ? $obj['n1:Approved'] : null,
                'approveDegree' => isset($obj['n1:ApproveDegree']) ? $obj['n1:ApproveDegree'] : null,
                'parentCode' => isset($obj['n1:ParentCode']) ? $obj['n1:ParentCode'] : null,
                'parentType' => isset($obj['n1:ParentType']) ? $obj['n1:ParentType'] : null,
                'name' => isset($obj['n1:Name']) ? $obj['n1:Name'] : null,
                'sortName' => isset($obj['n1:SortName']) ? $obj['n1:SortName'] : null,
                'dateCreated' => isset($obj['n1:DateCreated']) ? $obj['n1:DateCreated'] : null,
                'dateModified' => isset($obj['n1:DateModified']) ? $obj['n1:DateModified'] : null,
                'dateDeleted' => isset($obj['n1:DateDeleted']) ? $obj['n1:DateDeleted'] : null,
                'atuCode' => isset($obj['n1:ATUCode']) ? $obj['n1:ATUCode'] : null,
                'villageCode' => isset($obj['n1:VillageCode']) ? $obj['n1:VillageCode'] : null,
                'streetCode' => isset($obj['n1:StreetCode']) ? $obj['n1:StreetCode'] : null,
                'postIndex' => isset($obj['n1:PostIndex']) ? $obj['n1:PostIndex'] : null,
                'postOfficeCode' => isset($obj['n1:PostOfficeCode']) ? $obj['n1:PostOfficeCode'] : null,
                'isBuild' => isset($obj['n1:ForBuild']) ? $obj['n1:ForBuild'] : null,
            ];
            $i++;
            if ($i % 100 == 0) {
                Console::updateProgress($i, $total, $this->getMemoryUsageMsg());
            }
        }
        Console::updateProgress($total, $total, $this->getMemoryUsageMsg());
        $prepared = isset($output['env:Envelope']['env:Body']['n1:AddressObjectDataFile']['n1:InfoPrepareDate']) ? $output['env:Envelope']['env:Body']['n1:AddressObjectDataFile']['n1:InfoPrepareDate'] : null;
        $this->msg('Finished processing data');
        return ['items' => $items, 'prepared' => $prepared];
    }
}