<?php

namespace console\modules\vzd\actions\base;

use common\modules\vzd\models\VzdCode;

abstract class VzdBaseAtuAction extends VzdBaseSmallAction
{
    /**
     * This method syncs data from VZD xml files: AW_RAJONS, AW_NOVADS, AW_PILSETA, AW_PAGASTS
     * @param $link
     */
    protected function syncATU($link, $listName)
    {
        $data = $this->getData($link, $listName);
        $this->msg('Inserting data objects');
        $this->insertVzdDataObjects($data);
        $this->msg('Inserting ATU Codes');
        $this->insertATUcodes($data['items']);

    }

    protected function insertATUcodes($data)
    {
        $db = \Yii::$app->db;
        $sql = "INSERT INTO vzd_code (type, code, object_code) VALUES";
        foreach ($data as $obj) {
            if ($db->quoteValue($obj['atuCode'])) {
                $type = VzdCode::TYPE_ATU;
                $code = $db->quoteValue($obj['atuCode']);
                $objectCode = $db->quoteValue($obj['code']);
                // type, code, object_code
                $sql .= "({$type}, {$code}, {$objectCode}),";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE `code` = VALUES(`code`), `type` = VALUES(`type`);';
        $db->createCommand($sql)->execute();
    }
}