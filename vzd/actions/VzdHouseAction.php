<?php

namespace console\modules\vzd\actions;

use console\modules\vzd\actions\base\VzdBaseLargeAction;

class VzdHouseAction extends VzdBaseLargeAction
{
    /**
     * Syncs Vzd Houses
     */
    public function run()
    {
        $links = $this->getLinks();
        $this->msg('Syncing Houses');
        $this->syncHouse($links->AddressObjectDataResponse->AddressObjectDataResult->HouseDataLink);
        $this->msg('Finished syncing Houses');
    }

    /**
     * This method syncs data from VZD xml files: AW_NLIETA
     * @param $link
     */
    protected function syncHouse($link)
    {
        $insertHouseInfo = function($data){
            $db = \Yii::$app->db;
            $sql = "INSERT INTO vzd_nlieta (object_code, post_index, post_office_code, is_build) VALUES";
            foreach ($data as $obj) {
                $objectCode = $db->quoteValue($obj['code']);
                $postIndex = $obj['postIndex'] ? $db->quoteValue($obj['postIndex']) : "''";
                $postOfficeCode = $obj['postOfficeCode'] ? $db->quoteValue($obj['postOfficeCode']) : "''";
                $isBuild = $obj['isBuild'] == 'Y' ? 1 : 0;
                // object_code, post_index, post_office_code, is_build
                $sql .= "({$objectCode}, {$postIndex}, {$postOfficeCode}, {$isBuild}),";
            }
            $sql = substr($sql, 0, -1);
            $sql .= ' ON DUPLICATE KEY UPDATE `post_index` = VALUES(`post_index`), `post_office_code` = VALUES(`post_office_code`), `is_build` = VALUES(`is_build`);';
            $db->createCommand($sql)->execute();
        };
        $this->getDataLarge($link, $insertHouseInfo, 'Inserting House information');
    }
}