<?php

namespace console\modules\vzd\actions;

use mgcode\commandLogger\LoggingTrait;
use yii\base\Action;
use common\components\ConvexHull;
use yii\db\Query;

class GenerateGeometryAction extends Action
{
    use LoggingTrait;

    /**
     * Action that generates polygon for given objects id from vzd table
     * @param $id
     */
    public function run($id)
    {
        $code = (new Query())->select(['code'])->from('vzd')->where(['code' => $id])->one();
        if (!$code) {
            $this->msg("No object found with code: {$id}");
            return;
        }
        $coordinates = $this->getCoordinates($code);
        if (empty($coordinates)) {
            $this->msg("No coordinates found for object with code: {$id}");
            return;
        }
        $hull = new ConvexHull($coordinates);
        $geom = $hull->getHullPoints();
        if (count($geom) < 3) {
            $this->msg("Not enough points to create polygon. Object code: {$id}");
            return;
        }
        $code = \Yii::$app->db->quoteValue($id);
        $center = $this->getCenter($geom);
        $long = $center['long'];
        $lat = $center['lat'];
        \Yii::$app->db->createCommand("INSERT INTO vzd_coordinates (`code`, `lat`, `long`) VALUES ({$code}, {$lat}, {$long}) ON DUPLICATE KEY UPDATE `lat` = VALUES(`lat`), `long` = VALUES(`long`)")->execute();
        $geom[] = $geom[0];
        $polygon = $this->getPolygonString($geom);
        $sql = "INSERT INTO vzd_geometry (`code`, `geometry`) VALUES ";
        $sql .= "({$code}, ST_PolygonFromText('POLYGON(({$polygon}))', 4326))";
        $sql .= ' ON DUPLICATE KEY UPDATE `geometry` = VALUES(`geometry`);';
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * @param $code
     * @param null $coordinates
     * @return array|null
     */
    protected function getCoordinates($code, $coordinates = null)
    {
        if (!$coordinates) {
            $coordinates = [];
        }
        $object = (new Query())->select(['lat', 'long'])->from('vzd_coordinates')->where(['code' => $code])->one();
        if ($object && $object['long'] && $object['lat']) {
            $coordinates[] = [$object['long'], $object['lat']];
        }
        $children = (new Query())->select(['code'])->from('vzd')->where(['parent_code' => $code])->all();
        foreach ($children as $child) {
            $coordinates = $this->getCoordinates($child['code'], $coordinates);
        }
        return $coordinates;
    }

    protected function getPolygonString($polygon)
    {
        $string = '';
        foreach ($polygon as $coordinates) {
            $string .= "{$coordinates[0]} {$coordinates[1]},";
        }
        $string = substr($string, 0, -1);
        return $string;
    }

    protected function getCenter($polygon)
    {
        $minLat = 90.0;
        $maxLat = -90.0;
        $minLong = 180.0;
        $maxLong = -180.0;
        foreach ($polygon as $point) {
            $pointLat = $point[1];
            $pointLong = $point[0];
            if ($pointLat < $minLat) {
                $minLat = $pointLat;
            }
            if ($pointLat > $maxLat) {
                $maxLat = $pointLat;
            }
            if ($pointLong < $minLong) {
                $minLong = $pointLong;
            }
            if ($pointLong > $maxLong) {
                $maxLong = $pointLong;
            }
        }
        $lat = $maxLat - (($maxLat - $minLat) / 2.0);
        $long = $maxLong - (($maxLong - $minLong) / 2.0);
        return ['long' => $long, 'lat' => $lat];
    }
}