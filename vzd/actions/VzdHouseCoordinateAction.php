<?php

namespace console\modules\vzd\actions;

use console\modules\vzd\actions\base\VzdBaseAction;

class VzdHouseCoordinateAction extends VzdBaseAction
{
    /**
     * Syncs Vzd House Coordinates
     */
    public function run()
    {
        $links = $this->getLinks('Apartment', true, true);
        $this->msg('Syncing House Coordinates');
        $this->syncHouseCoordinate($links->AddressObjectDataResponse->AddressObjectDataResult->HouseCoordinateDataLink);
        $this->msg('Finished syncing House Coordinates');
    }

    /**
     * This method syncs data from VZD xml files: AW_EKA_GEO
     * @param $link
     */
    protected function syncHouseCoordinate($link)
    {
        $reader = new \XMLReader();
        $reader->open($link);
        $i = 0;
        $this->msg('Processing data');
        $data['items'] = [];
        $data['prepared'] = isset($output['env:Envelope']['env:Body']['n1:AddressObjectDataFile']['n1:InfoPrepareDate']) ? $output['env:Envelope']['env:Body']['n1:AddressObjectDataFile']['n1:InfoPrepareDate'] : null;
        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'n1:ObjectCoordinate') {
                $xml = simplexml_load_string($reader->readOuterXml());
                $xml->registerXPathNamespace('n1', 'http://ivis.eps.gov.lv/XMLSchemas/100007/AddressRegistry/v1-0');
                $part = $xml->xpath("//n1:ObjectCoordinate");
                $obj = $part[0]->children("n1", true);
                $data['items'][] = [
                    'code' => isset($obj->ARCode) ? $obj->ARCode->__toString() : null,
                    'x' => isset($obj->XValue) ? $obj->XValue->__toString() : null,
                    'y' => isset($obj->YValue) ? $obj->YValue->__toString() : null,
                ];
                $i++;
                if ($i % 500 == 0) {
                    $this->msg("{$i} records processed");
                }
                if ($i % 20000 == 0) {
                    $this->msg('Inserting 20000 records');
                    $this->msg('Inserting data objects');
                    $this->insertVzdDataObjectCoordinates($data);
                    unset($data['items']);
                    $data['items'] = [];
                    gc_collect_cycles();
                }
            }
        }
        if (!empty($data['items'])) {
            $this->msg('Inserting data objects');
            $this->insertVzdDataObjectCoordinates($data);
        }
        $this->msg('Finished processing data');
    }

    protected function insertVzdDataObjectCoordinates($data)
    {
        $preparedCoordinates = [];
        foreach ($data['items'] as $obj) {
            if (isset($obj['x']) && isset($obj['y'])) {
                $coordinates = $this->LKSToLatLon($obj['x'], $obj['y']);
                $preparedCoordinates[] = [
                    'code' => $obj['code'],
                    'lat' => $coordinates['lat'],
                    'long' => $coordinates['long']
                ];
            }
        }
        $this->insertCoordinates($preparedCoordinates);
    }

    /**
     * Originally: https://gist.github.com/laacz/2597627
     * Function converts LKS92 coords to latitude and longitude.
     * Based on converter by Charles L. Taylor (http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html),
     * by removing stuff, I did not need.
     * Converted by Kaspars Foigts (laacz.lv)
     * Parameters: x and y in LKS-92 system (example: 506835 un 312644).
     * Result: an array with lat and long (example: [56.957350385732, 24.112385718036]).
     * Actual address for these coords is: Elizabetes iela 41/43, Rīga, LV-1010, Latvia
     */
    protected function LKSToLatLon($x, $y)
    {
        /* Ellipsoid model constants (actual values here are for WGS84) */
        $UTMScaleFactor = 0.9996;
        $sm_a = 6378137.0;
        $sm_b = 6356752.314;
        $sm_EccSquared = 6.69437999013e-03;
        $x -= 500000.0;
        // Pirmā atšķirība no WGS84 - Kilometriņš šurpu, kilometriņš turpu.
        $y -= -6000000.0;
        $x /= $UTMScaleFactor;
        $y /= $UTMScaleFactor;
        // Otrā atšķirība no WGS84 - Centrālais meridiāns ir citur.
        $lambda0 = deg2rad(24);
        /* Precalculate n (Eq. 10.18) */
        $n = ($sm_a - $sm_b) / ($sm_a + $sm_b);
        /* Precalculate alpha_ (Eq. 10.22) */
        /* (Same as alpha in Eq. 10.17) */
        $alpha_ = (($sm_a + $sm_b) / 2.0)
            * (1 + (pow($n, 2.0) / 4) + (pow($n, 4.0) / 64));
        /* Precalculate y_ (Eq. 10.23) */
        $y_ = $y / $alpha_;
        /* Precalculate beta_ (Eq. 10.22) */
        $beta_ = (3.0 * $n / 2.0) + (-27.0 * pow($n, 3.0) / 32.0)
            + (269.0 * pow($n, 5.0) / 512.0);
        /* Precalculate gamma_ (Eq. 10.22) */
        $gamma_ = (21.0 * pow($n, 2.0) / 16.0)
            + (-55.0 * pow($n, 4.0) / 32.0);
        /* Precalculate delta_ (Eq. 10.22) */
        $delta_ = (151.0 * pow($n, 3.0) / 96.0)
            + (-417.0 * pow($n, 5.0) / 128.0);
        /* Precalculate epsilon_ (Eq. 10.22) */
        $epsilon_ = (1097.0 * pow($n, 4.0) / 512.0);
        /* Now calculate the sum of the series (Eq. 10.21) */
        $phif = $y_ + ($beta_ * sin(2.0 * $y_))
            + ($gamma_ * sin(4.0 * $y_))
            + ($delta_ * sin(6.0 * $y_))
            + ($epsilon_ * sin(8.0 * $y_));
        /*  Precalculate ep2 */
        $ep2 = (pow($sm_a, 2.0) - pow($sm_b, 2.0))
            / pow($sm_b, 2.0);
        /* Precalculate cos (phif) */
        $cf = cos($phif);
        /* Precalculate nuf2 */
        $nuf2 = $ep2 * pow($cf, 2.0);
        /* Precalculate Nf and initialize Nfpow */
        $Nf = pow($sm_a, 2.0) / ($sm_b * sqrt(1 + $nuf2));
        $Nfpow = $Nf;
        /* Precalculate tf */
        $tf = tan($phif);
        $tf2 = $tf * $tf;
        $tf4 = $tf2 * $tf2;
        /* Precalculate fractional coefficients for x**n in the equations
           below to simplify the expressions for latitude and longitude. */
        $x1frac = 1.0 / ($Nfpow * $cf);
        $Nfpow *= $Nf;   /* now equals Nf**2) */
        $x2frac = $tf / (2.0 * $Nfpow);
        $Nfpow *= $Nf;   /* now equals Nf**3) */
        $x3frac = 1.0 / (6.0 * $Nfpow * $cf);
        $Nfpow *= $Nf;   /* now equals Nf**4) */
        $x4frac = $tf / (24.0 * $Nfpow);
        $Nfpow *= $Nf;   /* now equals Nf**5) */
        $x5frac = 1.0 / (120.0 * $Nfpow * $cf);
        $Nfpow *= $Nf;   /* now equals Nf**6) */
        $x6frac = $tf / (720.0 * $Nfpow);
        $Nfpow *= $Nf;   /* now equals Nf**7) */
        $x7frac = 1.0 / (5040.0 * $Nfpow * $cf);
        $Nfpow *= $Nf;   /* now equals Nf**8) */
        $x8frac = $tf / (40320.0 * $Nfpow);
        /* Precalculate polynomial coefficients for x**n.
           -- x**1 does not have a polynomial coefficient. */
        $x2poly = -1.0 - $nuf2;
        $x3poly = -1.0 - 2 * $tf2 - $nuf2;
        $x4poly = 5.0 + 3.0 * $tf2 + 6.0 * $nuf2 - 6.0 * $tf2 * $nuf2
            - 3.0 * ($nuf2 * $nuf2) - 9.0 * $tf2 * ($nuf2 * $nuf2);
        $x5poly = 5.0 + 28.0 * $tf2 + 24.0 * $tf4 + 6.0 * $nuf2 + 8.0 * $tf2 * $nuf2;
        $x6poly = -61.0 - 90.0 * $tf2 - 45.0 * $tf4 - 107.0 * $nuf2
            + 162.0 * $tf2 * $nuf2;
        $x7poly = -61.0 - 662.0 * $tf2 - 1320.0 * $tf4 - 720.0 * ($tf4 * $tf2);
        $x8poly = 1385.0 + 3633.0 * $tf2 + 4095.0 * $tf4 + 1575 * ($tf4 * $tf2);
        /* Calculate latitude */
        $lat = $phif + $x2frac * $x2poly * ($x * $x)
            + $x4frac * $x4poly * pow($x, 4.0)
            + $x6frac * $x6poly * pow($x, 6.0)
            + $x8frac * $x8poly * pow($x, 8.0);
        /* Calculate longitude */
        $lon = $lambda0 + $x1frac * $x
            + $x3frac * $x3poly * pow($x, 3.0)
            + $x5frac * $x5poly * pow($x, 5.0)
            + $x7frac * $x7poly * pow($x, 7.0);
        return ['lat' => rad2deg($lat), 'long' => rad2deg($lon)];
    }
}